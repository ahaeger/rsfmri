% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/fsl_fugue.R
\name{fsl_fugue}
\alias{fsl_fugue}
\title{Do field map correction for EPI.}
\usage{
fsl_fugue(infile, outfile = NULL, mapfile, dwell_time)
}
\arguments{
\item{infile}{The functinal image to correct.}

\item{outfile}{The name of the file to output.}

\item{mapfile}{The fieldmap to use for correction}

\item{dwell_time}{The EPI dwell (echo-spacing) time in seconds.}
}
\description{
Do field map correction for EPI.
}
