#' Do the entire chain of FSL-commands for field map creation.
#'
#' @param magnitude_map The magnitude map file.
#' @param phase_map The phase map file.
#' @param echo_diff The echo difference in ms.
#'
#' @export
fsl_make_fieldmap <- function(magnitude_map, phase_map, echo_diff) {
    mag_path <- normalizePath(magnitude_map)
    phase_path <- normalizePath(phase_map)

    mag_norm <- fsl_fieldmap_prep(mag_path)
    mag_extract <- fsl_bet(mag_norm[1], robust = TRUE, bias_cleanup = TRUE)
    mag_erode <- fsl_maths_erode(mag_extract[1])

    pha_norm <- fsl_reorient2std(phase_path)
    fm_prep <- fsl_prepare_fieldmap(mag_erode[1], pha_norm[1],
                                    echo_diff = echo_diff)

    list(mag_norm = mag_norm[1],
         mag = mag_extract[1],
         fmap = fm_prep)
}


#' Do the entire chain of FSL-commands for field map generation with AP/PA maps
#'
#' @param ap_map The AP map
#' @param pa_map The PA map
#' @param readout_time The readout time
#' @param debug If TRUE prints command output.
#'
#' @export
fsl_make_fieldmap_pe <- function(ap_map, pa_map, readout_time, debug = FALSE) {
    in_ap <- normalizePath(ap_map)
    in_pa <- normalizePath(pa_map)

    base_dir <- dirname(in_ap)
    merge_out_fn <- file.path(base_dir, "merged_map")

    r_args <- c("-t", merge_out_fn, in_ap, in_pa)
    r_out <- system2("fslmerge", r_args, stdout = debug, stderr = debug,
                     env = "FSLOUTPUTTYPE=NIFTI_GZ")

    if (!file.exists(paste0(merge_out_fn, ".nii.gz"))) {
        stop("Could not merge maps.")
    }

    datain <- rbind(
        c(0, -1, 0, readout_time),
        c(0,  1, 0, readout_time)
    )
    dt_fn <- file.path(base_dir, "datain.txt")
    write.table(datain, dt_fn, row.names = FALSE, col.names = FALSE,
                quote = FALSE)

    tp_fout <- file.path(base_dir, "fmap_pre")
    tp_iout <- file.path(base_dir, "unwarped")

    tp_args <- c(paste0("--imain=", merge_out_fn),
                 paste0("--datain=", dt_fn),
                 "--config=b02b0.cnf",
                 paste0("--fout=", tp_fout),
                 paste0("--iout=", tp_iout))

    tp_out <- system2("topup", tp_args, stdout = debug, stderr = debug,
                      env = "FSLOUTPUTTYPE=NIFTI_GZ")
    if (!file.exists(paste0(tp_fout, ".nii.gz")) |
        !file.exists(paste0(tp_iout, ".nii.gz"))) {
        stop("Could not generate pre-fieldmap.")
    }

    fieldmap <- file.path(base_dir, "fmap")
    fieldmap_mag <- file.path(base_dir, "fmap_mag")
    mag_bet <- file.path(base_dir, "fmap_mag_brain")

    math_arg <- c(tp_fout, "-mul", "6.28", fieldmap)
    math_out <- system2("fslmaths", math_arg, stdout = debug, stderr = debug,
                       env = "FSLOUTPUTTYPE=NIFTI_GZ")

    mean_arg <- c(merge_out_fn, "-Tmean", fieldmap_mag)
    mean_out <- system2("fslmaths", mean_arg, stdout = debug, stderr = debug,
                         env = "FSLOUTPUTTYPE=NIFTI_GZ")

    bet_out <- system2("bet2", c(fieldmap_mag, mag_bet),
                       stdout = debug, stderr = debug,
                       env = "FSLOUTPUTTYPE=NIFTI_GZ")

    if (!file.exists(paste0(fieldmap, ".nii.gz")) |
        !file.exists(paste0(fieldmap_mag, ".nii.gz")) |
        !file.exists(paste0(mag_bet, ".nii.gz"))) {
        stop("Fieldmap generation failed.")
    }

    list(mag_norm = fieldmap_mag,
         mag = mag_bet,
         fmap = fieldmap)
}

