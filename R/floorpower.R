#' @title Extended flooring
#' @description Apply floor function with a specified power of 10.
#' @details  Extend the floor function by adding the ability to target an
#' arbitrary power of ten. So you can go  e.g. from 3412 to 3000 For negative
#' values supplied to power, precision might be limited
#'
#' @param input_values Numeric values of some sort, can be a vector, list, or a
#'   single values.
#' @param power The power of 10 to target. 0 the default will lead to the same
#'   behaviour as the default floor() function.
#' @return The input Values floored to the specified power.
#' @examples
#' x <- c(12365.53, 17608.78, 37860.34, 99967.98)
#' floorpower(x, 3)
#' @export
floorpower <- function(input_values, power = 0) {
  if (!is.numeric(power)) {
    stop("The power argument should be numeric.")
  } else if (!is.numeric(input_values)) {
    stop("Input values should be numeric")
  }
  target_power <- 10 ^ power
  floored_values <- base::floor(input_values / target_power)
  floored_values <- floored_values * target_power

  floored_values
}
