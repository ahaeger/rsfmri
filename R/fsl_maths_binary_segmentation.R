#' Binarise a segmentation with fslmaths.
#'
#' @param infile The file to work on.
#' @param outfile Optional output file name.
#' @param threshold The threshold to use for binarisation.
#'
#' @export
fsl_maths_binary_segmentation <- function(infile, outfile = NULL,
                                          threshold = 0.5) {
    infile <- normalizePath(infile)

    if (is.null(outfile)) {
        new_file <- .infix(infile, "bin")
    } else {
        new_file <- outfile
    }

    args_vector <- c(infile, "-thr", threshold, "-bin", new_file)

    r <- system2("fslmaths", args_vector, stdout = FALSE, stderr = FALSE,
                 env = "FSLOUTPUTTYPE=NIFTI_GZ")

    .returnobject(new_file, r)
}
